from pyparrot.Minidrone import Mambo
from pyparrot.DroneVision import DroneVision
import threading
import cv2
import time
import os

os.system("rm -f ~/anaconda3/lib/python3.7/site-packages/pyparrot/images/*.png") # remove old images cache

class UserVision:
    def __init__(self, vision):
        self.index = 0
        self.vision = vision

    def save_pictures(self, args):
        img = self.vision.get_latest_valid_picture()

        if (img is not None):
            #filename = "test_image_%06d.png" % self.index
            cv2.imshow("", img)
            cv2.waitKey(1)
            self.index +=1
            #print(self.index)

            # TODO: insert all vision processing here

# connect to the Mambo
mambo = Mambo("e0:14:d0:63:3d:d0", use_wifi=True)
print("Trying to connect to mambo now...")
success = mambo.connect(num_retries=3)

if (success):
    # get drone state information
    print("sleeping")
    mambo.smart_sleep(1)
    mambo.ask_for_state_update()
    mambo.smart_sleep(1)

    # opening vision module
    mamboVision = DroneVision(mambo, is_bebop=False, buffer_size=30)
    userVision = UserVision(mamboVision)
    mamboVision.set_user_callback_function(userVision.save_pictures, user_callback_args=None)
    success = mamboVision.open_video()

    if (success):
        print("Vision successfully started!")
        mambo.smart_sleep(5)

        # TODO: insert all drone commands and PID updates here

        # done doing vision demo
        print("Ending the sleep and vision")
        mamboVision.close_video()
        mambo.smart_sleep(5)

    print("disconnecting")
    mambo.disconnect()
